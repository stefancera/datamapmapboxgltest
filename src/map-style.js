import {fromJS} from 'immutable';
import MAP_STYLE from '../initialExampleData/map-style-basic-v8.json';

// For more information on data-driven styles, see https://www.mapbox.com/help/gl-dds-ref/
export const dataLayer = fromJS({
  id: 'data',
  source: 'incomeByState',
  type: 'fill',
  interactive: true,
  paint: {
    'fill-color': {
      property: 'percentile',
      stops: [
        [0, '#71E85E'],
        [1, '#41E029'],
        [2, '#2EB319'],
        [3, '#258F14'],
        [4, '#1C6B0F'],
        [5, '#12470A'],
        [6, '#0E3608'],
        [7, '#092405'],
        [8, '#051203']
      ]
    },
    'fill-opacity': 0.8
  }
});

export const defaultMapStyle = fromJS(MAP_STYLE);
