/* global window, fetch */
import React, {Component} from 'react';
import {render} from 'react-dom';
import MapGL from 'react-map-gl';

import {defaultMapStyle, dataLayer} from './map-style.js';
import {fromJS} from 'immutable';
import {json as requestJson} from 'd3-request';
import * as Papa from "papaparse";
import {range} from 'd3-array';
import {scaleQuantile} from 'd3-scale';

const token = process.env.MAPBOX_ACCESS_TOKEN; // eslint-disable-line

if (!token) {
  throw new Error('Please specify a valid mapbox token');
}

export default class App extends Component {

  state = {
    mapStyle: defaultMapStyle,
    year: 2014,
    data: null,
    hoveredFeature: null,
    viewport: {
      latitude: 40,
      longitude: -100,
      zoom: 3,
      bearing: 0,
      pitch: 0,
      width: 500,
      height: 500
    }
  };

  componentDidMount() {
    window.addEventListener('resize', this._resize);
    this._resize();
    let loadData = this._loadData;

    Papa.parse("../myData/DATA_mhi_us_zcta_2014.csv", {
      download: true,
      header: true,
      fastMode: true,
      dynamicTyping: true,
      complete: function (csvData) {
        requestJson('myData/us_state_zcta_2014.geojson', (error, response) => {
          if (!error) {
            const {features} = response;
            const matchZCTA = function (f, r) {
              return r.data.find(zcta => zcta.id === f.properties.id)
            };
            const mhiF = f => f.properties.mhi;

            const scale = scaleQuantile().domain(features.map(mhiF)).range(range(9));
            features.forEach(f => {
              const zctaData = matchZCTA(f, csvData);
              f.properties.value = zctaData.mhi;
              f.properties.zcta = zctaData.zcta;
              f.properties.percentile = scale(zctaData.mhi);
            });
            console.log("finishedMergingData");
            loadData(response);
          }
        });
      }
    });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._resize);
  }

  _resize = () => {
    this.setState({
      viewport: {
        ...this.state.viewport,
        width: this.props.width || window.innerWidth,
        height: this.props.height || window.innerHeight
      }
    });
  };

  _loadData = (data) => {
    const mapStyle = defaultMapStyle
    // Add geojson source to map
      .setIn(['sources', 'incomeByState'], fromJS({type: 'geojson', data}))
      // Add point layer to map
      .set('layers', defaultMapStyle.get('layers').push(dataLayer));
    console.log('Drawing');
    this.setState({data, mapStyle});
  };

  _onViewportChange = viewport => this.setState({viewport});

  _onHover = event => {
    const {features, srcEvent: {offsetX, offsetY}} = event;
    const hoveredFeature = features && features.find(f => f.layer.id === 'data');

    this.setState({hoveredFeature, x: offsetX, y: offsetY});
  };

  _renderTooltip() {
    const {hoveredFeature, year, x, y} = this.state;

    return hoveredFeature && (
      <div className="tooltip" style={{left: x, top: y}}>
        <div>Median Household Income: ${hoveredFeature.properties.value} in ZIP Code: {hoveredFeature.properties.zcta}</div>
      </div>
    );
  }

  render() {

    const {viewport, mapStyle} = this.state;

    return (
      <div>
        <MapGL
          {...viewport}
          mapStyle={mapStyle}
          onViewportChange={this._onViewportChange}
          mapboxApiAccessToken={token}
          onHover={this._onHover} >

          {this._renderTooltip()}

        </MapGL>

      </div>
    );
  }

}
